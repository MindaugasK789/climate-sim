"""ver 0.8.1

blender 2.69.0
python 3.3

ver 0.1.0
    Parasyta klase WeatherSim su funkcijomis. Funkcijos kolkas nieko nedaro,
    isskyrus __init__, kurioje sukurti kintamieji self.sce, self.own ir
    paleidziama pagrindine funkcija - temp_values.
    Kodas strukturizuotas tai, kaip turetu atrodyti galutiniame variante.

ver 0.2.0
    Funkcijoje __init__ sukuriamas zodynas self.fdict , kuriame bus talpinamos
        vertes reikalongos temperaturos skaiciavimams.
    I funkcija __init__ prideti kintamieji:
        self.poly_num - objekto poligonu skaicius.
        self.salbedo - dirvos albedo reiksme (kolkas nenaudojama, reikalinga
            skaiciuojant temperaturos verte).
    Parasytas funkcijos get_faces turinys. Funkcija skirta surinkti
        temperaturos skaiciavimams reikalinga informacija. Informacija
        sudedama i self.fdict zodyna:
            [0] poligono temperaturos verte.
            [1] poligono lokali normale.
            [2] poligono lokali pozicija.
            [3] poligono vertex 1.
            [4] poligono vertex 2.
            [5] poligono vertex 3.
            [6] poligonui priskiriama ramuneles objekta (0 kol ju nera).
            [7] poligono albedo reiksme.

ver 0.2.1
    Pataisymai:
        Pataisyta versija skripto aprase.
        __init__ funkcijoje iskvieciama funkcija get_faces.

ver 0.3.0
    Importuojami moduliai:
        math
        time modulio time funkcija
    Parasyta funkcijos temp_values dalis atliekanti kiekviemo poligono
        pavirsiau temperaturos verciu skaiciavimus. Tam prideti reikalingi
        kintamieji i __init__ funkcija:
            self.tmin - minimali poligono temperatura.
            self.tmax - maksimali poligono temperatura.
            self.sun_luminocity - sviesos saltinio spinduliuojamos energijos
                kiekis vatais.
            self.sil_talpa - poligono silumine talpa, J/(g*K).
            self.smass - poligono mase, gramais.
            self.semision - poligono siluminis spinduliavimas.
            self.time0 - laiko verte skaiciavimams, sekundemis.
            self.time_scale - laiko vertes padidinimui (simuliacijos
                pagreitinimui).
    Funkcija temp_values iskvieciama funkcijoje __init__.

ver 0.4.0
    Importuojamas modulis colorsys.
    Funkcijoje __init__ sukuriamas zodynas self.cdict, spalva atitinkanciu
        sarasu talpinimui.
    Parasytas fukcijos temp_to_color turinys. Funkcija sudaro spalva
        atitinkanciu sarasu zodyna temperaturos vertems, ribose [self.tmin;
        self.tmax]. Sarasai talpinami i self.cdict zodyna.
    Funkcija temp_to_color iskvieciama funkcijoje __init__.

ver 0.5.0
    Funkcijoje temp_values prideta dalis keicianti poligono spalva atitinkamai
        pagal temperaturos verte.

ver 0.6.0
    Importuojamas modulis random.
    Funkcijoje __init__ sukuriami kintamieji self.walbedo ir self.balbedo -
        atitinkamai baltos ir juodos ramuneliu albedo reiksmes.
    Parasytas funkcijos add_daisy turinys. Funkcija prideda po viena ramuneles
        objekta ant poligono ir apskaiciuoja to objekto albedo reiksme.
        Objektas sudarytas is baltu ir juodu ramuneliu, nuo 0 iki 4 vienos
        rusies, isviso - 4. Pridetas objektas talpinamas i self.fdict[fnum][6].
        Pagal rmuneliu sudeti apskaiciuota albedo reiksme talpinama
        i self.fdict[fnum][7].
    Funkcija add_daisy iskvieciama funkcijoje __init__.

ver 0.7.0
    Funkcijoje temp_values prideta dalis keicianti ramuneles objekto mesh jei
        ramuneles poligono temperaturos verte nera ramuneles temperaturos
        ribose. Si dalis isskirta i atskira for cikla, nes to reikes
        tolimesniem skripto pakeitimams.

ver 0.8.0
    Funkcijoje temp_values prie ramuneliu objekto keitimo for ciklo pridetas
        ramuneliu ciklo tikrinimas. Tai reikalinga, kad ramuneliu objektas butu
        keiciamas reciau nei, kad paleidziamas skriptas. Tam funkcijoje
        __init__ prideti kintamieji self.daisy_cycle - skripto paleidimu
        skaicius iki bus pakeisto ramuneles, self.daisy_cycle_day skripto
        paleidimo numeris. Butu galima naudoti laika, taciau naudojant laiko
        pagreitinima ramuneliu objektai butu keiciami ne pakankamai daznai.

ver 0.8.1
    Prideta komentaru.
"""


import bge
import math
import random
import colorsys
from time import time as t


class WeatherSim(bge.types.KX_GameObject):

    def __init__(self, own):
        self.sce = bge.logic.getCurrentScene()
        self.own = bge.logic.getCurrentController().owner

        self.poly_num = self.own.meshes[0].numPolygons

        self.fdict = {}  # Zodynas poligonu parametrams.
        self.cdict = {}  # Spalvu zodynas temperaturu vertems.

        # Temperturos verciu islaikymui ribose.
        self.tmin = -20
        self.tmax = 60
        self.sun_luminocity = 3.846 * 10**26  # vatais.

        # Dirvos parametrai:
        self.sil_talpa = 1.48  # J/(g*K)
        self.smass = 136000*10  # g
        self.salbedo = 0.9
        self.semision = 0.95
        self.salbedo = 0.9

        # Demesio! Naudojamos albedo reiksmes yra gaunamos pagal (1-albedas),
        # nes skaiciuojama kiek sugeriama ne kiek atspindima.

        # Ramuneliu albedas.
        self.walbedo = 0.15
        self.balbedo = 1

        # Laiko paspartinimui skaiciavimuose, 1s = 1s*self.time_scale.
        self.time0 = t()
        self.time_scale = 60*60

        # Ramuneliu perdeliojimui kas self.daisy_cycle scripto paleidima.
        self.daisy_cycle = 1
        self.daisy_cycle_day = 0

        self.get_faces()
        self.add_daisy()
        self.temp_to_color()
        self.temp_values()  # Kad nuo pirmo logic tick prasidetu skaiciavimai.
        self.main = self.temp_values

    # Suranda visus objekto poligonus, ju vertex, apskaiciuoja lokalias
    # normalias ir pozicijas. Sudeda tai i self.fdict zodyna.
    #
    # zodyne:
    # 0 - temperaturos verte.
    # 1 - lokali normale.
    # 2 - lokali pozicija.
    # 3 - vertex 1.
    # 4 - vertex 2.
    # 5 - vertex 3.
    # 6 - ramuneles objektas arba 0 jei jo nera.
    # 7 - albedo reiksme. Dirvos jei nera ramuneliu.
    def get_faces(self):
        for fnum in range(self.poly_num):

            polygon = self.own.meshes[0].getPolygon(fnum)

            v1 = self.own.meshes[0].getVertex(0, polygon.v1)
            v2 = self.own.meshes[0].getVertex(0, polygon.v2)
            v3 = self.own.meshes[0].getVertex(0, polygon.v3)

            poly_local_normal = (v1.getNormal() +
                                 v2.getNormal() +
                                 v3.getNormal())/3

            poly_local_pos = (v1.XYZ +
                              v2.XYZ +
                              v3.XYZ)/3

            self.fdict[fnum] = [0,
                                poly_local_normal,
                                poly_local_pos,
                                v1,
                                v2,
                                v3,
                                0,
                                self.salbedo]

    # Prideda ramuneliu objektu ant poligonu, atsitiktinai balta arba juoda.
    #
    def add_daisy(self):
        # Numeris prie pavadinimo daisy reiskia baltu ramunelius skaisciu.
        # (ramuneles objektas, ramuneles objektas, temperaturos ribu sarasas).
        self.daisydict = {0: ('daisy_0', 'daisy_0', [-274, 13]),
                          1: ('daisy_a1', 'daisy_1b', [11, 19]),
                          2: ('daisy_2a', 'daisy_2b', [17, 22]),
                          3: ('daisy_3a', 'daisy_3b', [20, 25]),
                          4: ('daisy_4', 'daisy_4', [23, 5000])}

        for fnum in range(self.poly_num):
            wcount = random.randint(0, 4)
            if wcount in range(1, 4):
                type = self.daisydict[wcount][random.randint(0, 1)]
            else:
                type = self.daisydict[wcount][0]

            new = self.sce.addObject('empty_ob', self.own, 0)
            new.position = self.own.worldTransform * self.fdict[fnum][2]
            new.alignAxisToVect((self.own.worldTransform*self.fdict[fnum][1]) -
                                self.own.position)
            new.setParent(self.own)

            new.replaceMesh(type)
            new['type'] = type
            new['wcount'] = wcount

            self.fdict[fnum][6] = new
            self.fdict[fnum][7] = self.walbedo*wcount + self.balbedo*(4-wcount)

    # Sudaro spalva atitinkanciu sarasu zodyna.
    #
    def temp_to_color(self):
        h = 0.67  # HUE h reiksme atitinkanti tamsiai melyna spalva
        deltah = h/(abs(self.tmin) + abs(self.tmax))
        for t in range(self.tmin, self.tmax):
            self.cdict[t] = list(colorsys.hsv_to_rgb(h, 1, 1))
            self.cdict[t].append(1)  # Permatomumo kanalui.
            h -= deltah
        if h < 0:  # Nes float skaiciavimai duoda pakankamas paklaidas.
            h = 0
        self.cdict[self.tmax] = list(colorsys.hsv_to_rgb(h, 1, 1))
        self.cdict[self.tmax].append(1)

    # Apskaiciuoja temperaturos vertes, piesia vektorius, keicia poligono
    # spalva, keicia ramuneles priklausomai nuo temperaturos vertes.
    #
    def temp_values(self):
        delta_time = (t() - self.time0)*self.time_scale  # Sekundemis;
        self.time0 = t()
        for sun in self.sce.lights:
            # Tokia israiska tik, kad nereiktu papildomo indentation.
            if not sun.type == 2:
                continue

            distance_to_sun = (
                ((self.own.position[0] - sun.position[0])**2 +
                (self.own.position[1] - sun.position[1])**2 +
                (self.own.position[2] - sun.position[2])**2) **
                0.5
            )*10000000000  # metrais

            ditance_luminocity = (self.sun_luminocity /
                                 (4*3.14*distance_to_sun**2))  # W/m**2

            dist_lum_sph_joules = ditance_luminocity * delta_time  # J/m**2

            vec_center_to_sun = self.own.getVectTo(sun)[1]

            # Skaiciavimai temperaturos verciu pokyciams gauti.
            #
            for fnum in range(self.poly_num):

                poly_global_pos = (self.own.worldTransform *
                                   self.fdict[fnum][2])

                vec_center_to_poly = self.own.getVectTo(poly_global_pos)[1]

                vec_poly_normal = (self.own.worldTransform *
                                   self.fdict[fnum][1] -
                                   self.own.position)

                angle_normal_to_sun = vec_poly_normal.angle(vec_center_to_sun)
                angle_poly_center_sun = vec_center_to_poly.angle(
                    vec_center_to_sun)

                if (angle_normal_to_sun < 1.5707963267948966):
                    deltaQI = (dist_lum_sph_joules *
                               math.cos(angle_normal_to_sun) *
                               (self.salbedo+self.fdict[fnum][7]/2))  # J
                else:
                    deltaQI = 0  # J

                deltaQE = (self.semision*(5.67*10**(-8))*1 *
                           delta_time*((self.fdict[fnum][0]+273.15)**4))  # J

                self.fdict[fnum][0] += ((deltaQI - deltaQE) /
                                        (self.sil_talpa*self.smass))  # K

        for fnum in range(self.poly_num):

            # Islaiko temperaturos vertes tam tikrose ribose.
            #
            if self.fdict[fnum][0] > self.tmax:
                self.fdict[fnum][0] = self.tmax
            elif self.fdict[fnum][0] < self.tmin:
                self.fdict[fnum][0] = self.tmin

            # Keicia poligono spalva.
            #
            for v in self.fdict[fnum][3:6]:
                v.color = self.cdict[int(self.fdict[fnum][0])]

        # Keicia ramunele priklausomai nuo temperaturos vertes.
        #
        if self.daisy_cycle_day == self.daisy_cycle:
            self.daisy_cycle_day = -1
            for fnum in range(self.poly_num):
                # != 0, kad nereiktu papildomo if, kai norima isjung ramuneles,
                # planuojama, kad esant tam tikroms salygoms ramuneles objektas
                # gali buti panaikinamas.
                if self.fdict[fnum][6] != 0:
                    nuo = self.daisydict[self.fdict[fnum][6]['wcount']][2][0]
                    iki = self.daisydict[self.fdict[fnum][6]['wcount']][2][1]
                    wcount = self.fdict[fnum][6]['wcount']

                    if self.fdict[fnum][0] > iki:
                        wcount += 1
                        if wcount in self.daisydict:
                            self.fdict[fnum][6]['wcount'] = wcount
                            self.fdict[fnum][6].replaceMesh(
                                self.daisydict[wcount][random.randint(0, 1)])
                            self.fdict[fnum][7] = (self.walbedo*wcount +
                                                   self.balbedo*(4-wcount))
                    elif self.fdict[fnum][0] < nuo:
                        wcount -= 1
                        if wcount in self.daisydict:
                            # Sudubliuotask kodas,nes i isorini indentation
                            # perdejus reiktu arba papildomo if arba visu
                            # objektu mesh keisti net jei to nereik. Taip
                            # greiciau.
                            self.fdict[fnum][6]['wcount'] = wcount
                            self.fdict[fnum][6].replaceMesh(
                                self.daisydict[wcount][random.randint(0, 1)])
                            self.fdict[fnum][7] = (self.walbedo*wcount +
                                                   self.balbedo*(4-wcount))
        self.daisy_cycle_day += 1


def main(cont):
    own = cont.owner
    if not "init" in own:
        own["init"] = 1
        WeatherSim(own)
    else:
        own.main()
