Supaprastinkas Daisy world simuliacijos variantas. 

Kad skriptas veiktu pirma reikia betkoki mesh turincio
objekto (turetu buti sferos formos, bet veiks su betkuo).

Objektui, kuriam norima atlikti simuliacija logic editoriuje
turi buti sukurtas "Always" sensorius su ijungtu True level
trigering. Rekomenduojamas daznis - 60.

Simuliacija remiasi is sviesos saltinio gaunamos energijos ir
pasisavinamos energijos, skaiciavimais.